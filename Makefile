
## Definitions

PATH := .:$(PATH)

include config.sdrun

FASTQ_2 := $(subst _1.,_2.,$(FASTQ_1))
FASTA_1 := $(FASTQ_1:.fastq=.fasta)
FASTA_2 := $(FASTQ_2:.fastq=.fasta)

## targets

all:
	@make -s -j$(NCPU) fastq
	@make -s -j$(NCPU) fasta
	@[ -n "$(FASTQ_1)" ] && make runsoap\
	|| echo "Place fastq files and `make' again."

clean: ; $(RM) $(GRAPH_PREFIX).*

reset: ; $(RM) runsoap

usage help: README.md; cat $<;
	

fastq: $(FASTQ_BZ2:.bz2=)

fasta:  $(FASTA_1) $(FASTA_2)

runsoap: soap.config $(FASTQ_1) $(FASTQ_2) $(FASTA_1) $(FASTA_2)
	$(SOAPdenovo) all -s $< -K 25 -R -o $(GRAPH_PREFIX) -p$(NCPU)
	touch $@

soap.config: soap.config.template $(FASTQ_1)
	@( cat $<; \
	for i in $(FASTQ_1); do \
		s=`basename $$i _1.fastq`;\
		echo q1=$$i;\
		echo q2=$${s}_2.fastq;\
		echo f1=$${s}_1.fasta;\
		echo f2=$${s}_2.fasta;\
	done ) >$@


## implicit rules

%: %.bz2; bunzip2 $<

%.fasta: %.fastq
	sed -ne 'N;s/^@/>/p' $< >$@

