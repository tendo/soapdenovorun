### README sdrun

This packages is designed for semi-automatic run of
SOAPdenovo sequence assembler for NGS.


### Prerequisite

Prepare `SOAPdenovo` executable by placing binary
in the current directory, or set PATH appropriately.
Make sure `SOAPdenovo` can be run exactly by this name.


### How to use

1. `git clone https://tendo@bitbucket.org/tendo/soapdenovorun.git`
1. `cd soapdenovorun`
1. Place `*_[12].fastq file` or bzip2 archive of them
   in the current directory.
1. Check `config.sdrun` for setting and edit if needed.
1. Type `make` to prepare required files and to execute`SOAPdenovo`


### Contact

Toshinori Endo <endo-sdrun@ibio.jp>

2017.2.2